# asim-interview questions 

my interview questions

1.what is html?
html stands for hypertext makeup language.
html is used to create your own website and the content of page.

2.what is css?
css stands for cascading style sheet.
css is used to design or style  the website layout.

3.what is tag and element?
An element is a set of opening and closing tags"<body-element>" in use.
tags are labels you use to mark up the begining and end of an element.
tags are common to all <>-open tag,</>close tag.


4.difference between margin,padding and border ?

Padding : 
Clears an area around the content the padding is transparent.

Border : a border that goes around the padding and content.

Margin : clears an area outside the border. 
The margin is transparent.


5. Difference between visibility :hidden and display : none ?

Visibility :hidden ;

It is not visible but gets its original space.

Display : none ;
It is hidden and takes no space .
 

6.What is the purpose of z-index ?

The  z-index property specifies the stack order of an element."the tags one on the one ".

An element with greater stack order is always in front of an element with a lower stack order.

Note : "z  -index only works on positioned elements (position : absolute, position : relative, position : fixed and position : sticky)" .


7. Css are case sensitive or not ?

Css are case  -insensitive , except for parts that are not under the control of css.

Id and class attribute are case sensitive.
 

8.Difference between display :relative,absolute and fixed ?

Position : static; 
it is default is not positioned in any special way.
 It is always positioned in normal flow of the page.

Position : relative ;
 an element with position relative is positioned relative to its normal position , it relate the style what we have given.

Eg : left 30px.

Position : fixed ;
 
Position fixed is relative to view port which means it always stays in the same place even the page is scrolled.

Position : absolute ;
 position is relative to the nearest position ancestor.

Position : sticky ;
 
An element with position sticky is positioned based on the users scroll position.
 

9.why we using css ?

Css are used to create the layout of the website. 

10.how many ways to add css ?

There are three ways to add css in code:

Inline css: - To use inline styles, add the style attribute to the relevant element. 
The style attribute can contain any CSS property.

Internal css: The internal style is defined inside the <style> element, inside the head section.

External css : with an external style sheet we can write with the extension of .css.


11. How to add css ?

Example for internal css :

<style>
body {
  background-color: linen;
}  
</style>

.
Example for external css :

<link rel="stylesheet" type="text/css" href="mystyle.css">
Like this we need to add the external css and we need to write code separately.

Example for inline css : 
<h1 style="color:blue;text-align:center;">This is a heading</h1>.


12. Multiple class id and class selectors ?

Example of adding multiple class : 


We can add many class to a single class attribute .

<!DOCTYPE html>

<html>

<head>

<style>

P.intro {
    
color: blue;
    text-align: center;

}

.important {
    
background-color: yellow;

}

</style>

</head>

<body>

<p class="intro important">welcome</p>

</body>

</html>

To write css for id attribute we need to add #symbol before the styels.

Eg :
 
#asim { 
color : red ;
}


To write css for class attribute we need to add .symbol before the class attribute. 

Eg : .asim 
{
color : red ;
}
 

13.What is em and 1 em is equal to how many pixels ?
 
em:

Relative to the font-size of the element (2em means 2 times the size of the current font)
1em = 16px.


14. Why we use percentage ? 

If we use percentage it will be responsive and adapt the device width and height and it will show proper output.
 
15 . How to change the color of 4th or 2nd row ?

Using nth - child()selector :we can change  color of the row we need.

eg : 

p:nth-child(2) 
{
  background: red;
}


16. How to use multiple link in a single image ?

Using <map> we can do this. 

Eg :
 
<img src = "img.png" usemap = " somename">

<map name = "somename " >

<area shape =  " circle"  coords = "34,44,270,350 " href = " www.somelink.com">

<area shape =  " react"  coords = "34,44,270,350 " href = " www.somelink.com">

</map>


17. How to use media query ? 

Media query can be used to check the width and height of the viewport , resolution, orientation ( tab,phone,pc,lap)width and height of device .

Syntax :

@media screen and (min-width : 480px)

{

Body 
{ 
color : green ;
 }

}

Value and their uses :

All - used for all media type device.

Print - used for printers. 

Screen - used for computer screens, tab , smart phone , lap, pc.

Speech - used for screen readers that "reads" the page out load.


18 . What is the use of ! Important ?
 
! important property in css is used to provide more weight (importance ) than normal property .


19 . What is bootstrap ? 

Bootstrap is a framework to help you  design websites faster and easier. 
To make codes shorten.
 

20. What is model ?

Model plugin is a dialog box popup window that is displayed on top of the current page.
 

21.what is tabs ?

Tabs are perfect for single page web applications ,or for web pages capable of displaying different subjects.


22 . Difference between alert and confirm  alert ?
 
Alert :

Alert box is mainly used to communicate a message to the user and displays one ok button.

Confirm alert : 

Confirm alert is opposite to alert box . Confirm box has two buttons such as  true or false and ok or cancel. 
User can choose one button depending on the users choice .

23. difference between dropdown and selector ?
selector:
listbox shows many options to the user simultaneously and lets them pick  one or more.
dropdown: 
dropdownlsit lets them pick only one you can set selectionmode either to multiple or single.

24. what is curd ?
curd stands for c-create, u-update, r-read and d- delete.

25. what is ajax ?
update a web page without reloading the page.
read data from a web server after a webpage has loaded.
send data to a web server in the background.
ajax stands for asynchronous javascript and xml.
ajax is not a programming language.
ajax is  a technique for accessing web servers from a web page.

26. why do we use pagination ?
to sort the pages for easy access of users to display total page and page to be selected.

27. what is ascending sorting and decending sorting ?
ascending is a normal order of a to z. or 1 to 100.
decending is a reverse order of ascending order. z to a,or 100 to 1.

28. keywords of javascript ?
keywords are tokens that have special meaning in javascript.
break,case,catch,continue,debugger,default,delete,do,else,finally,for,function,if,in,retrun,var,void,while,switch,
this,throw,instanceof.

29. what are arithmetic operation ?
arithmetic operators perform arithmatic on numbers(literals or variables).
operator and their uses:
+ addition
- subtraction
* multiplication
/ division
% modulus(remainder)
++ increment
-- decrement.

30. what are the js data types?
javascript variables can hold many data types:
numbers(0 to 9).
string (it contains both character and numbers).
booleans (true or false ).
arrays(we can store multiple values inbetween square brackets[]).
objects(we can store multiple values using curly braces{}).
typeof (to find the of (string or number) a javascript variable).
undefined(a variable without a value has the value undefined. the type is  also undefined.)
empty values(an empty values has nothing to do with undefined an empty string has both a legal value and a type.)
eg:var car =" ";//the value is "",the typeof is "string".
null(in javascript null is "nothing". its data type of null is object
eg: var person= {asim:"21"}).
person = null;
you can empty an object by setting to null.


31. difference between undefined and null ?
type of undefined//undefined.
type of null //object.
null === undefined //false.
null === undefined//true.

32.difference between array and object ?
arrays:
arrays are placed between[],arrays are a special type of variable that is also mutable and can also be used to store a list of variables.

objects:
objects are stored between {},
objects represent a special data type that is mutable and can be used to store a collection of data (rather then just a single value). 

33. how to add script into html file?
to write code externally <script src ="filename.js"></script>.
to add code internally <script>your codes should be placed here.</script>.

34. how to add css into html ?
<link rel="stylesheet" type ="text/css" href="file.css">

35. what is localstorage ?
the localstorage property is read-only.we can store our values temprory.
syntax for saving data to localstorage :
localstorage.setItem("key","value");

syntax for reading data from localstorage:
var lastname = localstorage.getItem("key");

syntax for removing data:
localstorage.removeItem("key");

36.how many types of storages in javascript?
three types of storage:
local storage.
session storage.
cookies storage.

37.difference between local storage and local storage?
sessionstorage:
the data in sessionstorage is cleared when the page session ends.
localstorage:
 localstorage the data in localstorage doesnot expire.

syntax for session storage same as localstorage instade of localstorage the word session storage sholud be added.

sessionstorage.clear(); to remove all saved data from session.

38. what are events ?
all the different visitors actions that a web page can respond to are called events.
an event represents the precise moment when something happens.
the term "fires/fired" is often used with events.
mouse event:
click(): the function is executed when the user clicks on the html elements.
doubleclick(): the function is executed when the user double clicks on the html element.
mouseenter(): the function is executed when the mouse pointer enters the html element.
mouseleave(): the function is executed when the mouse pointer leaves the html element.
keyboard event:
keypress(),keydown(),keyup().
the above mentioned events are executed when the user press the following events in keyboard on the html elements. 
form events:
submit(),change(),focus(),blur().
the above mentioned events are executed when the user clicks the form, the following events on the html elements.
document/window events:
load(),resize(),scroll(),unload().
the above mentioned events are executed when the user clicks the document or window, the following events  on the html elements.

syntax for events: 
$(document).ready(function(){
$("p").click (function(){
$(this.hide();
});
});  

39. what is function ?
a method is a black of code which only runs  when it is called.
you can pass data known as parameters into a method.
methods are used to perform certain actions and they are also known as funtions.
syntax:
function myfunction(name of function)
{
your funtions will be here;
}

40. what are the data types in typescript ?
-boolean.
-number. 
-string.
-array.
-tuple (we can assign two data type in the same time.)
-enum: 
       enum allows us to declare a set of named constants. 
       eg: a collection of related values that can be numeric ot string values. 
  enum are in three types:
    numeric enum.
    string enum.
    heterogenous enum.
-unknown:
      it will be anything.
        eg: notsure : unknown = 4;
        notsure = "may be a string instead";  
-any:
    you can create an array of type any[] if you are not sure about the types of values that can contain this array.
-void:
     return type.
-null and undefined :
    null and undefined have their own types.
-never:
   the never type represents the type of values that never occur.
-object: 
    object is a type that represents the non-primitive type.
-type assertions:
   type assertions are a way to tell the compiler "trust me, i know what i'm doing".
-let:
    let keyword instead of javascript's var keyword.

41. difference between interface and class ?
-class:
   classes  are the fundamental entities used to create reusable components.
   it is a group of objects which have common properties.
   it can contain properties like fields, methods, constructors,etc.
   #usage:
      it is used for object creation, encapsulation for fields methods.
   #keyword:  
       we can create a class by using the class keyword.
   #compilation:
       a class cannot disappear during the compilation of code.
   #methods:
        the methods of class are used to perform a specific action.
   #constructor:
         a class can have  a constructor. 
   #access specifier:
         the member of a class can be public, protected, or private.
-interface:
   an interface defines a structrue which acts as a contract in our application.
   it containes only the declaration of the methods and fields, but not the implementation. 
   #usage:
      it is used to create a structrue for an entity.
   #keyword:
      we can create an interface by using the interface keyword.
   #compilation:
      interface completely disappeared during the compilation of code.
   #methods:
      the methods in an interface are purely abstract (the only declaration,not have a body).
   #access specifier:
      the members of an interface are always public.

42. can we use class as a data type like interface ?
    yes we can use class as a datatype.
    when we define a class we create a reference datatype and when we create
    a reference data type and when  we create an object we declare a variable
    of class datatype.
    eg:
       suppose there is a class count and we create an object obj then obj is a variable of count type.

43. why we use git ?
    git is a free and open source distributed version. 
    git is used to keep the access for every user in a team.  

44. how to create new repository /
    create a directory to contain the project.
    go into the new directory.
    type git init.
    write some code.
    type git add .
    type git commit.
45. how to push, add, commit command ?
    git push for push the project to git. 
    git add . for add the project to git. 
    git commit -m "cmt" for commit the project to git.

46. why we use vs code ?
    it will help to code quickly, it has many features to code easily and it is powerful develop tool.
 
47. how to terminal in vs code ?
     press keyboard keys ctrl+` to open terminal in vs code.
     in the top of the vs code there will be a option displayed as terminal click and go through the options and click the new terminal.

48. how to push pull and add using vs code ?
    click the branch icon in the vs code and there will be shown three dots above the input box click that dots 
    the push, pull and add will be shown here by using that we can push, pull and add.

49. what is components ?
    components are the most basic ui building block of an angular app.
    angular components are a subset of directives, always associated with a template.
    eg: 
       @Component({
        selector: 'app-name',
        templateUrl: './name.component.html',
        styleUrls: ['./name.component.css']
        })
50. what is interpolation binding ?
    interpolation refers to embedding expressions into marked up text, by default interpolation uses as its
    delimiter the double curly braces.{{name.value}}.

51. what is property binding ?
    property binding to set properties of target elements or directive @Input()decorators.
    properties are defined by DOM(document object model).
    property values however can change.
    syntax:
        <img[src]="itemImageUrl">
        property binding is done with in [] ="name".

52. what is event binding ?
    events are triggered by user input.
    binding to this events provides a way to get input form the user
    syntax:
          <button (click)="onclickme()">hi</button>
           event binding (click)="onclickme".

53. what is class binding ?
    class binding is used to set a class property of a view element.
    we can add and remove the css class names from an element's class attribute with class binding.
    single class binding:
    [class.foo]= "hasfoo".
    multiple class binding
    [class]= "classexp".
    class binding is used to bind the css class.

54. what is style binding ?
    style binding is used to set a style of a view element.
    we can set inline styles with style binding.
    single style binding:
    [style.width]="width"
    multi-style binding:
    [style]="styleexpr".
    style binding is used to bind styles.
55. what is two way binding ? 
    two-way binding gives your app a way to share data between a component class and its template.
    basics of two-way binding:
    two-way binding does two things.
    1.sets a specific element property.
    2.listens for an element change event.
    angular offers a special two-way data binding syntax for this purpose[()].
    the syntax combines the brackets of property binding [],with the parentheses of event binding,().
    eg:
     <input[(ngmodel)]="name" type="text">
     {{name}}
     in export class{
     public name ="";}

56. what is component interaction ?
    pass data from parent to child with input binding to child with input binding intercept input
    property changes with  a setter intercept input property changes with ngOnChanges().
    pass  data from parent to child with input binding with the use of @input() decorater.
    eg:
       export class HeroChildComponent{
       @Input()hero:Hero;

57. what is decorators ?
    using decorators we can configure and customise our classes at design time.
    they are just functions that can be used to add meta-data,properties or functions to the thing they are attached to.


 